<?php
// Se connecter à la base de données
include("db.php");
$request_method = $_SERVER["REQUEST_METHOD"];

include("fonction.php");

switch($request_method)
{
    case 'GET':
        if(!empty($_GET["id"]))
        {
            // Récupérer un seul produit
            $id = intval($_GET["id"]);
            getTicket($id);
        }
        else
        {
            // Récupérer tous les produits
            getTickets();
        }
        break;
    case 'POST':
        CreateTicket();
        break;
    case 'PUT':
        // Modifier un produit
        $id = intval($_GET["id"]);
        updateTicket($id);
        break;
    case 'DELETE':
        // Supprimer un produit
        $id = intval($_GET["id"]);
        deleteTicket($id);
        break;

    default:
        // Requête invalide
        header("HTTP/1.0 405 Method Not Allowed");
        break;
}

?>
