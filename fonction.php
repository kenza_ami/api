<?php

// avoir l'ensemble des tickets
function getTickets()
{
    global $conn;
    $query = "SELECT * FROM tickets";
    $response = array();
    $reponse = mysqli_query($conn,$query);

    while ($donnees = mysqli_fetch_assoc($reponse))
    {
        array_push($response,$donnees);
    }

    header('Content-Type: application/json');
    echo json_encode($response, JSON_PRETTY_PRINT);
}

// récupérer un ticket à partir de son id
function getTicket($id)
{
    global $conn;
    $query = "SELECT * FROM tickets where identifiant = $id";
    $reponse =  mysqli_query($conn,$query);
    $response = mysqli_fetch_assoc($reponse);
    header('Content-Type: application/json');
    echo json_encode($response, JSON_PRETTY_PRINT);
}


// création d'un ticket
function CreateTicket()
{
    global $conn;
    $description = $_POST["description"];
    $date = $_POST["date"];
    $severite = $_POST["severite"];
    $modified = date('Y-m-d H:i:s');

    $query = "INSERT INTO tickets (date, descip_probleme, severite, modified) VALUES ('".$date."','".$description."','".$severite."','".$modified."')";
    if(mysqli_query($conn, $query))
    {
        $response=array(
            'status' => 1,
            'status_message' =>'Ticket ajoute avec succes.'
        );
    }
    else
    {
        $response=array(
            'status' => 0,
            'status_message' =>'ERREUR!.'. mysqli_error($conn)
        );
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}

// mise à jour d'un ticket spécifique à partir de son id
function updateTicket($id){
    global $conn;
    $_PUT = array(); //tableau qui va contenir les données reçues
    parse_str(file_get_contents('php://input'), $_PUT);
    $description = $_PUT["description"];
    $date = $_PUT["date"];
    $severite = $_PUT["severite"];
    $modified = date('Y-m-d H:i:s');
    //construire la requête SQL
    $query="UPDATE tickets SET descip_probleme='".$description."', date='".$date."', severite='".$severite."', modified='".$modified."' WHERE identifiant =".$id;

    if(mysqli_query($conn, $query))
    {
        $response=array(
            'status' => 1,
            'status_message' =>'Ticket mis a jour avec succes.'
        );
    }
    else
    {
        $response=array(
            'status' => 0,
            'status_message' =>'Echec de la mise a jour de ticket. '. mysqli_error($conn)
        );

    }

    header('Content-Type: application/json');
    echo json_encode($response);
}
// suppression d'un ticket spécifique à partir de son id
function deleteTicket($id)
{
    global $conn;
    $query = "DELETE FROM tickets WHERE identifiant=".$id;
    if(mysqli_query($conn, $query))
    {
        $response=array(
            'status' => 1,
            'status_message' =>'ticket supprime avec succes.'
        );
    }
    else
    {
        $response=array(
            'status' => 0,
            'status_message' =>'La suppression du ticket a echoue. '. mysqli_error($conn)
        );
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}